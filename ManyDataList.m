%Pathways to folders containing data
 paths=[];



paths{1} =   '/Volumes/Vault2Data/Kevin/Tone_Rearing/test/Bitcoin/layerII_III';
paths{2} =   '/Volumes/Vault2Data/Kevin/Tone_Rearing/PV-cre/Harvey/layerII_III';
paths{3} =   '/Volumes/Vault2Data/Kevin/Tone_Rearing/PV-cre/Ben/layerII_III';




%File names for various experiments. Psignal files that correspond to commented out expnames will not be processed.

%input.expname{1} = 'mid-high-awake';
input.expname = [];
input.expname{1} = 'unknown';
%input.expname{2} = 'mid-freq_awake';

%Psignal files
psignalfiles=[];

  
psignalfiles{1,1} = 'ART_lucy_2017_10_26_Phys_2.mat';
psignalfiles{1,2} = 'ART_lucy_2017_10_26_Phys_1.mat';
psignalfiles{2,1} = 'ART_maya_2017_10_26_Phys_2.mat';
psignalfiles{2,2} = 'ART_maya_2017_10_26_Phys_1.mat';
psignalfiles{3,1} = 'ART_sasha_2017_10_26_Phys_3.mat';
psignalfiles{3,2} = 'ART_sasha_2017_10_26_Phys_2.mat';
psignalfiles{4,1} = 'ART_lucy_2017_10_21_Phys_2.mat';
psignalfiles{4,2} = 'ART_lucy_2017_10_21_Phys_1.mat';
psignalfiles{5,1} = 'ART_maya_2017_10_21_Phys_2.mat';
psignalfiles{5,2} = 'ART_maya_2017_10_21_Phys_1.mat';
psignalfiles{6,1} = 'ART_sasha_2017_10_21_Phys_2.mat';
psignalfiles{6,2} = 'ART_sasha_2017_10_21_Phys_1.mat';
