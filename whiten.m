function [X] = whiten(X)
X=double(X);
X = bsxfun(@minus, X, mean(X));
A = X'*X;
[V,D] = eig(A);
X = X*V*diag(1./(diag(D)+eps).^(1/2))*V';
end