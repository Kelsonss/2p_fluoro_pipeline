function opts = get_options_from_xml(xmlpath)
%Parse the 'Experiment.xml' file at xmlpath and construct options struct opts from
%its contents.
opts = get_default_options;
xml = xmlread(xmlpath);
wavelengths = xml.item(0).item(19);
opts.sections = 1
opts.numframes = ceil(str2num(wavelengths.getAttribute('timepoints'))/opts.sections);
opts.stopFrame = opts.numframes;
lsm = xml.item(0).item(27);
opts.dimX = str2num(lsm.getAttribute('pixelX'));
opts.dimY = str2num(lsm.getAttribute('pixelY'));
%The green channel will be either the first or second
%channel. Whether or not the channel is enabled depends on
%whether the corresponding gain is positive. This section of
%codes determines which channel is the green channel.
pmt = xml.item(0).item(29);
gainA = pmt.getAttribute('gainA');
gainB = pmt.getAttribute('gainB');
assert(xor(strcmp(gainA, '0'), strcmp(gainB, '0')));
if ~strcmp(gainA, '0')
    opts.greenChannel = 1;
elseif ~strcmp(gainB, '0')
    opts.greenChannel = 2;
end
%As above, but for the red channel.
gainC = pmt.getAttribute('gainC');
gainD = pmt.getAttribute('gainD');
%assert(xor(strcmp(gainC, '0'), strcmp(gainD, '0')));
if ~strcmp(gainC, '0')
    opts.redChannel = 3;
elseif ~strcmp(gainD, '0')
    opts.redChannel = 4;
end
function opts = get_default_options
%Default input options for loading raw image sequences from ThorImage.
opts = struct;
%Image dimensions
opts.dimX = 512;
opts.dimY = 512;
%Channel index
opts.redChannel = 4;
opts.greenChannel = 4;
opts.templateFrame = 1;
opts.numframes = 5000; %Needs to be changed based on length of seq
%File formatting options
opts.format = {'uint16', [opts.dimX, opts.dimY, 4], 'channels'};
opts.startFrame = 1;
opts.stopFrame = opts.numframes;
opts.echo = true;
opts.profile = true;